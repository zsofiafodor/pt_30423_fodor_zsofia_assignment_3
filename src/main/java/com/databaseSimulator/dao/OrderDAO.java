package com.databaseSimulator.dao;

import java.io.FileNotFoundException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Stream;

import com.assignment3.databaseSimulator.model.Client;
import com.assignment3.databaseSimulator.model.Order;
import com.assignment3.databaseSimulator.model.Product;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * 
 * @author Fodor Zsofia
 * CRUD operations for Order
 *
 */

public class OrderDAO extends GenericDAO<Order>{

	private static Connection orderConnection;
	private Statement orderStatement;

	public OrderDAO(Connection con) {
		OrderDAO.orderConnection = con;
		try {
			this.orderStatement = con.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @return list of orders, from order table
	 * @throws SQLException
	 */
	
	public ArrayList<Order> getOrderList() throws SQLException {
		ResultSet rs = orderStatement.executeQuery("SELECT * FROM assignment3.order");

		ArrayList<Order> ordersList = new ArrayList<Order>();
		while (rs.next()) {
			Order myNewOrder = new Order();
			myNewOrder.setOrderID(rs.getInt("orderID"));
			myNewOrder.setClientID(rs.getInt("clientID"));
			myNewOrder.setProductID(rs.getInt("productID"));
			myNewOrder.setQuantity(rs.getInt("quantity"));
			ordersList.add(myNewOrder);
		}
		return ordersList;
	}
	
	/**
	 * 
	 * @return array of client IDs
	 * @throws SQLException
	 */

	public int[] getClientsIDs() throws SQLException {
		ResultSet rs = orderStatement.executeQuery("SELECT clientID FROM client");
		int[] result;
		int size = 0, poz = 0;
		if (rs.last()) {
			size = rs.getRow();
			rs.beforeFirst(); 
		}
		result = new int[size];
		while (rs.next()) {
			result[poz] = rs.getInt("clientID");
			poz++;
		}
		return result;
	}

	/**
	 * 
	 * @return array of product IDs
	 * @throws SQLException
	 */
	
	public int[] getProductsIDs() throws SQLException {
		ResultSet rs = orderStatement.executeQuery("SELECT productID FROM product");
		int[] result;
		int size = 0, poz = 0;
		if (rs.last()) {
			size = rs.getRow();
			rs.beforeFirst();
		}
		result = new int[size];
		while (rs.next()) {
			result[poz] = rs.getInt("productID");
			poz++;
		}
		return result;
	}
	
	/**
	 * 
	 * @return the largest ID from table order +1
	 * @throws SQLException
	 */

	public int getLastID() throws SQLException {
		ResultSet rs = orderStatement.executeQuery("SELECT max(orderID) FROM assignment3.order");
		int lastID = 0;
		while (rs.next()) {
			lastID = rs.getInt(1);
		}
		int newID = lastID + 1;
		return newID;
	}

	/**
	 * 
	 * @param id, id of order we want to search 
	 * @return the order which has the id given as parameter
	 * @throws SQLException
	 */
	
	public Order searchID(int id) throws SQLException {
		
		Order myOrder = null;
		ArrayList<Order> orderList = getOrderList();
		Iterator<Order> myIterator = orderList.iterator();
		while (myIterator.hasNext()) {
			Order currentOrder = myIterator.next();
			if (currentOrder.getOrderID() == id) {
				myOrder = currentOrder;
				break;
			}
		}
		return myOrder;
	}
	
	/**
	 * 
	 * @param nr, number of document to be generated
	 * @throws DocumentException
	 * @throws SQLException
	 * @throws IOException
	 */

	public void generateReportOnOrders(Integer nr) throws DocumentException, SQLException, IOException {
		
		Document document = new Document();
		String name = "ReportOrders" + nr + ".pdf";
		PdfWriter.getInstance(document, new FileOutputStream(name));
		document.open();
		PdfPTable table = new PdfPTable(4);
		addTableHeader(table);
		addRows(table);
		document.add(table);
		document.close();
		
	}


	/**
	 * 
	 * @param table, table we want to insert in report
	 * @throws SQLException
	 */
	
	private void addRows(PdfPTable table) throws SQLException {
		
		ArrayList<Order> productList = getOrderList();
		for(int i = 0; i < getOrderList().size(); i++) {
			table.addCell(String.valueOf(productList.get(i).getOrderID()));
			table.addCell(String.valueOf(productList.get(i).getClientID()));
			table.addCell(String.valueOf(productList.get(i).getProductID()));
			table.addCell(String.valueOf(productList.get(i).getQuantity()));
		}
	}
	
	/**
	 * 
	 * @param table, table we want to insert in the report
	 */

	private void addTableHeader(PdfPTable table) {
		
		Stream.of("orderID", "clientID", "productID", "quantity")
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle));
	        table.addCell(header);
	    });
		
	}
	
	/**
	 * 
	 * @param name, name of client who is ordering
	 * @param product, product the client is ordering
	 * @param quantity, the quantity the client wants to order
	 * @param billNumber, number of bill to be generated
	 * @throws SQLException
	 * @throws FileNotFoundException
	 * @throws DocumentException
	 */

	public void insertNewOrder(String name, String product, Integer quantity, Integer billNumber) throws SQLException, FileNotFoundException, DocumentException {
		
			Double price;
			ProductDAO prDAO = new ProductDAO(orderConnection);
			Product myProduct = prDAO.searchProduct(product);
			ClientDAO cDao = new ClientDAO(orderConnection);
			Client myClient = cDao.searchByName(name);
			
			if(myClient != null && myProduct != null) {
				int productStock = myProduct.getStock();
				if (quantity <= productStock) {
					price = quantity * myProduct.getPrice();
					myProduct.setStock(productStock - quantity);
					Update(myProduct);
					Order myOrder = new Order();
					myOrder.setOrderID(getLastID());
					myOrder.setClientID(myClient.getClientID());
					myOrder.setProductID(myProduct.getProductID());
					myOrder.setQuantity(quantity);
					Insert(myOrder);
					GenerateBill(name, price, billNumber, quantity, myProduct.getName());
					
				} 
				else {
					GenerateNotEnoughOnStock(billNumber, name);
					System.out.println("Quantity bigger than stock! -> ERROR ");
				}
					
			}
			else {
				System.out.println("|| no such client || no such product");
			}
			
		
	}
	/**
	 * 
	 * @param billNr number of bill to be generated
	 * @param name name of the client who wants to order
	 */
	
	private void GenerateNotEnoughOnStock(Integer billNr, String name) {
		
		Document document = new Document();
		String myname = "Bill" + billNr + ".pdf";
		try {
			PdfWriter.getInstance(document, new FileOutputStream(myname));
		} catch (FileNotFoundException | DocumentException e) {
			e.printStackTrace();
		}
		document.open();
		try {
			document.add( new Paragraph( "Name: " + name ) );
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		try {
			document.add( new Paragraph( "Not enough on stock" ) );
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
		
	}

	/**
	 * 
	 * @param string name of the product
	 * @param quantity the desired quantity of that product
	 * @param name, name of client who ordered
	 * @param price, the price the client had to pay
	 * @param billNr, number of bill document
	 * @throws FileNotFoundException
	 * @throws DocumentException
	 */

	private void GenerateBill(String name, Double price, Integer billNr, Integer quantity, String string) throws FileNotFoundException, DocumentException {

		Document document = new Document();
		String myname = "Bill" + billNr + ".pdf";
		PdfWriter.getInstance(document, new FileOutputStream(myname));
		document.open();
		document.add( new Paragraph( "Name: " + name ) );
		document.add( new Paragraph( "Order product: " + string ) );
		document.add( new Paragraph( "Desired quantity: " + quantity ) );
		document.add( new Paragraph( "Total price: " + price ) );
		document.close();
	}

	/**
	 * 
	 * @param id, id of order we want to delete
	 * @throws SQLException
	 */
	
	public void DeleteOrder(Integer id) throws SQLException {
		
		
			Order orderToBeDeleted = searchID(id);
			if (orderToBeDeleted != null) {
				System.out.println("Deleting order with id: " + id);
				delete(orderToBeDeleted);
			} else
				System.out.println("ERROR when deleting order. Order inexistent! ");
	}
	
	
}
