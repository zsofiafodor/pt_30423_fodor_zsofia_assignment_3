package com.databaseSimulator.dao;


import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Stream;
import com.assignment3.databaseSimulator.model.Product;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


/**
 * 
 * @author Fodor Zsofia
 * CRUD operations for Product table
 *
 */

public class ProductDAO extends GenericDAO<Product>{

	
	private static Connection productConnection;
	private Statement productStatement;

	public ProductDAO(Connection con) {
		
		this.productConnection = con;
		try {
			this.productStatement = con.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * @return the greatest id from product table incremented by 1
	 * @throws SQLException
	 */
	
	public int getLastID() throws SQLException {
		ResultSet rs = productStatement.executeQuery("SELECT max(productID) FROM product");

		int lastID = 0;
		while (rs.next()) {
			lastID = rs.getInt(1);
		}

		int newID = lastID + 1;
		return newID;
	}
	
	/**
	 * 
	 * @param name, name of product
	 * @return true if valid name/false if name is not valid
	 */
	
	public boolean ValidateName(String name) {

		if (name == "")
			return false;
		if (name.matches("[ a-zA-Z]+") == false)
			return false;
		return true;
	}
	
	/**
	 * 
	 * @param name, name of product we want to create
	 * @param price, price of product
	 * @param stock, the amount we have of it 
	 * @throws SQLException
	 */
	
	public void InsertProduct(String name, Double price, Integer stock) throws SQLException {
		
		if(ValidateName(name)) {
			Product newProduct = new Product();
			Product existingProduct = searchProduct(name);
			if(existingProduct == null) {
				newProduct.setProductID(getLastID());
				newProduct.setName(name);
				newProduct.setPrice(price);
				newProduct.setStock(stock);
				Insert(newProduct);
			}
			else {
				existingProduct.setStock(existingProduct.getStock() + stock);
				Update(existingProduct);
			}
			
		}
		else 
			System.out.println("Bad input");
	}
	
	
	/**
	 * 
	 * @return list of products from product table
	 * @throws SQLException
	 */
	
	public ArrayList<Product> getProductList() throws SQLException{
		
		ResultSet res = productStatement.executeQuery("SELECT * FROM assignment3.product");
		ArrayList<Product> productList = new ArrayList<Product>();
		while(res.next()) {
			Product newP = new Product();
			newP.setProductID(res.getInt("productID"));
			newP.setName(res.getString("name"));
			newP.setPrice(res.getDouble("price"));
			newP.setStock(res.getInt("stock"));
			productList.add(newP);
		}
		return productList;
	}
	
	/**
	 * 
	 * @param name, name of product we want to search for
	 * @return the product which has the name we wanted to search or null if it does not exist
	 * @throws SQLException
	 */
	
	public Product searchProduct(String name) throws SQLException {
		Product myProduct = null;
		
		ArrayList<com.assignment3.databaseSimulator.model.Product> productList = getProductList();
		Iterator<Product> it = productList.iterator();
		while(it.hasNext()) {
			Product currProduct = it.next();
			if(currProduct.getName().contentEquals(name)) {
				myProduct = currProduct;
				break;
			}
		}
		return myProduct;
	}
	
	
	/**
	 * 
	 * @param name, name of the product we want to delete
	 */
	
	public void deleteProduct(String name) {
		try {
			Product newProd = searchProduct(name);
			if(newProd != null)
				delete(newProd);
		}catch(Exception e) {
			System.out.println("Error deleting desired product");
		}
	}
	
	/**
	 * 
	 * @param nr, number of report document
	 * @throws DocumentException
	 * @throws SQLException
	 * @throws IOException
	 */
	
	public void generateReportOnProducts(Integer nr) throws DocumentException, SQLException, IOException {
		
		Document document = new Document();
		String name = "ReportProducts" + nr + ".pdf";
		PdfWriter.getInstance(document, new FileOutputStream(name));
		document.open();
		PdfPTable table = new PdfPTable(4);
		addTableHeader(table);
		addRows(table);
		document.add(table);
		document.close();
		
	}

	/**
	 * 
	 * @param table, table we will insert in report pdf
	 * @throws SQLException
	 */
	
	private void addRows(PdfPTable table) throws SQLException {
		
		ArrayList<Product> productList = getProductList();
		for(int i = 0; i < getProductList().size(); i++) {
			table.addCell(String.valueOf(productList.get(i).getProductID()));
			table.addCell(productList.get(i).getName());
			table.addCell(String.valueOf(productList.get(i).getPrice()));
			table.addCell(String.valueOf(productList.get(i).getStock()));
		}
	}
	
	/**
	 * 
	 * @param table, table we will insert in report pdf
	 */

	private void addTableHeader(PdfPTable table) {
		
		Stream.of("productID", "name", "price", "stock")
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle));
	        table.addCell(header);
	    });
		
	}
	
	/**
	 * 
	 * @param prod, product we want to update
	 * @param newStock, the new stock
	 * @param newPrice, the new price
	 */

	public void UpdateProduct(Product prod, Integer newStock, Double newPrice) {
		
		prod.setPrice(newPrice);
		prod.setStock(newStock);
		Update(prod);
	}

	
	/**
	 * 
	 * @param productID, id of product we are searching for
	 * @return the product we were searching for or null if it does not exist
	 * @throws SQLException
	 */
	
	public Product searchID(int productID) throws SQLException {

		Product myProduct = null;
		
		ArrayList<com.assignment3.databaseSimulator.model.Product> productList = getProductList();
		Iterator<Product> it = productList.iterator();
		while(it.hasNext()) {
			Product currProduct = it.next();
			if(currProduct.getProductID() == productID) {
				myProduct = currProduct;
				break;
			}
		}
		return myProduct;
	}
}
