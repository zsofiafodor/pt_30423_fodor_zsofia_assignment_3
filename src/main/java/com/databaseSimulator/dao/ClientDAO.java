package com.databaseSimulator.dao;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.stream.Stream;

import com.assignment3.databaseSimulator.model.Client;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.util.Date;
import java.sql.Timestamp;


/**
 * 
 * @author Fodor Zsofi
 * 
 * CRUD operations for Client
 *
 */
public class ClientDAO extends GenericDAO<Client> {

	/**
	 * 
	 */
	private static Connection clientConnection;
	private Statement clientStatement;

	public ClientDAO(Connection con) {
		ClientDAO.clientConnection = con;
		try {
			this.clientStatement = con.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param nr, the number of document to be generated
	 * @throws DocumentException
	 * @throws SQLException
	 * @throws IOException
	 */
	public void generateReportOnClients(Integer nr) throws DocumentException, SQLException, IOException {
		
		Document document = new Document();
		String name = "ReportClients" + nr + ".pdf";
		PdfWriter.getInstance(document, new FileOutputStream(name));
		document.open();
		PdfPTable table = new PdfPTable(3);
		addTableHeader(table);
		addRows(table);
		document.add(table);
		document.close();
		
	}

	/**
	 * 
	 * @param table, the table we want to insert in our pdf file
	 * @throws SQLException
	 */
	private void addRows(PdfPTable table) throws SQLException {
		
		ArrayList<Client> clientsList = getClientsList();
		for(int i = 0; i < getClientsList().size(); i++) {
			table.addCell(String.valueOf(clientsList.get(i).getClientID()));
			table.addCell(clientsList.get(i).getName());
			table.addCell(clientsList.get(i).getAddress());
		}
	}
	
	/**
	 * 
	 * @param table, the table we want to insert in our pdf file
	 */

	private void addTableHeader(PdfPTable table) {
		
		Stream.of("clientID", "name", "address")
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle));
	        table.addCell(header);
	    });
		
	}

	/**
	 * 
	 * @param name, name of the client to be inserted
	 * @param address, address of the client
	 * @throws SQLException
	 */
	public void insertNewClient(String name, String address) throws SQLException {
		
		if(ValidateName(name)  && searchByName(name) == null) {
			Client newClient = new Client();
			newClient.setClientID(getLastID());
			newClient.setName(name);
			newClient.setAddress(address);
			Insert(newClient);
		}
		else 
			System.out.println("Bad input || Client " + name+ " already exists");
	}
	
	
	/**
	 * 
	 * @return newID, which is the last ID from our table +1
	 * @throws SQLException
	 */
	
	public int getLastID() throws SQLException {
		ResultSet rs = clientStatement.executeQuery("SELECT max(clientID) FROM client");

		int lastID = 0;
		while (rs.next()) {
			lastID = rs.getInt(1);
		}

		int newID = lastID + 1;
		return newID;
	}
	
	
	/**
	 * 
	 * @param name, name of the client
	 * @return true if the name is a valid one/ false if name is not valild
	 */
	
	public boolean ValidateName(String name) {

		if (name == "")
			return false;
		if (name.matches("[ a-zA-Z]+") == false)
			return false;
		return true;
	}
	
	/**
	 * 
	 * @return list of clients from client table
	 * @throws SQLException
	 */
	
	public ArrayList<Client> getClientsList() throws SQLException {
		ResultSet rs = clientStatement.executeQuery("SELECT * FROM assignment3.client");

		ArrayList<Client> clientsList = new ArrayList<Client>();
		while (rs.next()) {
			Client newClient = new Client();
			newClient.setClientID(rs.getInt("clientID"));
			newClient.setName(rs.getString("name"));
			newClient.setAddress(rs.getString("address"));
			clientsList.add(newClient);
		}

		return clientsList;
	}
	
	/**
	 * 
	 * @param name, name of the client
	 * @return client whose name is the name given as parameter
	 * @throws SQLException
	 */
	
	public Client searchByName(String name) throws SQLException {
		Client myClient = null;

		ArrayList<com.assignment3.databaseSimulator.model.Client> clientsList = getClientsList();
		Iterator<Client> myIterator = clientsList.iterator();
		while (myIterator.hasNext()) {
			Client currentClient = myIterator.next();
			if (currentClient.getName().contentEquals(name) ) {
				myClient = currentClient;
				break;
			}
		}
		return myClient;
	}
	

	/**
	 * 
	 * @param name, name of client to be deleted
	 * @param address, address of the client deleted
	 */
	
	public void deleteClient(String name, String address) {
		try {
			Client myNewClient=searchByName(name);
			if(myNewClient!=null) 
				delete(myNewClient);
		}catch (Exception e) {
			System.out.println("Error at deleting element");
		}
	}
	
}
