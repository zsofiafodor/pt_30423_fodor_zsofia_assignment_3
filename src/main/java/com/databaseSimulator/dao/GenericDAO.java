package com.databaseSimulator.dao;

import java.lang.reflect.Field;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.assignment3.databaseSimulator.connector.ConnectionFactory;

/**
 * GenericDAO is the entity where the CRUD operations are written for all types
 * of objects, which will be used later for client, order and product entities
 * @author Fodor Zsofi
 *
 * 
 */

public class GenericDAO<T> {

	/**
	 * 
	 * @param genericObject, which can be of type client, order or product, depending on what kind og object we want
	 * to insert in which table of our DB
	 */
	
	public void Insert(Object genericObject) {
		String tableName = genericObject.getClass().getSimpleName();
		StringBuilder query = new StringBuilder();
		StringBuilder values = new StringBuilder();
		query.append("Insert into assignment3." + tableName + " (");
		Field[] allFields = genericObject.getClass().getDeclaredFields();
		try {
			for (int i = 0; i < allFields.length - 1; i++) {
				allFields[i].setAccessible(true);
				query.append(allFields[i].getName());
				query.append(", ");
				Object value = allFields[i].get(genericObject);
				String fieldType = allFields[i].getType().getSimpleName();
				if (fieldType.equals("String"))
					values.append("\"" + value + "\"");
				else
					values.append(value);
				values.append(", ");
			}
			int lastFieldIndex = allFields.length - 1;
			allFields[lastFieldIndex].setAccessible(true);
			query.append(allFields[lastFieldIndex].getName());
			Object value = allFields[lastFieldIndex].get(genericObject);
			String fieldType = allFields[lastFieldIndex].getType().getSimpleName();
			if (fieldType.equals("String"))
				values.append("\"" + value + "\"");
			else
				values.append(value);
			query.append(") values (").append(values).append(" )");
		} catch (Exception e) {
			System.out.println("Error at inserting");
		}
		try {
			Connection con = ConnectionFactory.getConnection();
			PreparedStatement prepInsertStatement = con.prepareStatement(query.toString());
			prepInsertStatement.executeUpdate();
			con.close();
			prepInsertStatement.close();
		} catch (Exception e) {
			System.out.println("Exception when executing insert query");
		}
	}
	
	/**
	 * 
	 * @param genericObject, object to be deleted from our DB
	 */

	public void delete(Object genericObject) {
		String tableName = genericObject.getClass().getSimpleName();
		StringBuilder query = new StringBuilder();
		query.append("Delete from assignment3." + tableName + " where ");
		Field[] allFields = genericObject.getClass().getDeclaredFields();
		Field firstField = allFields[0];
		firstField.setAccessible(true);
		String fieldName = firstField.getName();
		query.append(fieldName).append(" = ");
		try {
			Object value = firstField.get(genericObject);
			query.append(value);
		} catch (Exception e) {
			System.out.println("Error at getting id value");
		}
		try {
			Connection con = ConnectionFactory.getConnection();
			PreparedStatement prepDeleteStatement = con.prepareStatement(query.toString());
			prepDeleteStatement.executeUpdate();
			con.close();
			prepDeleteStatement.close();
		} catch (Exception e) {
			System.out.println("Exception when executing delete query");
		}
	}

	
	/**
	 * 
	 * @param genericObject, object to be updated in our DB
	 */
	
	public void Update(Object genericObject) {
		String tableName = genericObject.getClass().getSimpleName();
		StringBuilder query = new StringBuilder();
		query.append("update assignment3." + tableName + " set ");
		Field[] allFields = genericObject.getClass().getDeclaredFields();
		try {
			for (int i = 0; i < allFields.length - 1; i++) {
				allFields[i].setAccessible(true);
				query.append(allFields[i].getName());
				query.append(" = ");
				Object value = allFields[i].get(genericObject);
				String fieldType = allFields[i].getType().getSimpleName();
				if (fieldType.equals("String"))
					query.append("\"" + value + "\"");
				else
					query.append(value);
				query.append(", ");
			}
		} catch (Exception e) {
			System.out.println("Error at updating");
		}
		int lastFieldIndex = allFields.length - 1;
		allFields[lastFieldIndex].setAccessible(true);
		query.append(allFields[lastFieldIndex].getName());
		query.append(" = ");
		try {
			Object value = allFields[lastFieldIndex].get(genericObject);
			String fieldType = allFields[lastFieldIndex].getType().getSimpleName();
			if (fieldType.equals("String"))
				query.append("\"" + value + "\"");
			else
				query.append(value);
		} catch (Exception e) {
			System.out.println("Error at updating");
		}
		
		query.append(" where ");
		Field firstField = allFields[0];
		firstField.setAccessible(true);
		String fieldName = firstField.getName();

		query.append(fieldName).append(" = ");

		try {
			Object value = firstField.get(genericObject);
			query.append(value);
		} catch (Exception e) {
			System.out.println("Error at getting id value");
		}
		try {
			Connection con = ConnectionFactory.getConnection();
			PreparedStatement prepUpdateStatement = con.prepareStatement(query.toString());
			prepUpdateStatement.executeUpdate();
			con.close();
			prepUpdateStatement.close();
		} catch (Exception e) {
			System.out.println("Exception when executing update query");
		}
	}

}
