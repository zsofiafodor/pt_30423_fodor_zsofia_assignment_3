package com.assignment3.databaseSimulator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

import com.assignment3.databaseSimulator.FileManagemenet.ReadFromFile;
import com.assignment3.databaseSimulator.connector.ConnectionFactory;
import com.databaseSimulator.dao.ClientDAO;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;

/**
 * App is the main class of the project
 * where the project itself is run
 * @author Fodor Zsofia
 *
 */
public class App 
{
    public static void main( String[] args ) throws ClassNotFoundException, SQLException, DocumentException, IOException
    {
        if(args.length != 1) {
        	System.err.println("Required parameters: <input_path>");
        }
        Connection con = ConnectionFactory.getConnection();
		if(con == null)
		{
			System.out.println("Cannot connect to database!");
		}
		File in = new File(args[0]);
		ReadFromFile myfile = new ReadFromFile(in, con);	
    }
    
	
}
