package com.assignment3.databaseSimulator.FileManagemenet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

import com.databaseSimulator.dao.ClientDAO;
import com.databaseSimulator.dao.OrderDAO;
import com.databaseSimulator.dao.ProductDAO;
import com.itextpdf.text.DocumentException;

/**
 * 
 * @author Fodor Zsofia
 * Reads from file given as input
 *
 */

public class ReadFromFile {

	private File in;
	private Connection myCon;
	private Integer clientReports = 1;
	private Integer orderReports = 1;
	private Integer productReports = 1;
	private Integer billNumber = 1;
	
	
	/**
	 * 
	 * @param input, input file
	 * @param con, our connection to the DB
	 * @throws SQLException
	 * @throws DocumentException
	 * @throws IOException
	 * 
	 */
	
	public ReadFromFile(File input, Connection con) throws SQLException, DocumentException, IOException {
		this.in = input;
		this.myCon = con;
		
		Scanner fileScanner = new Scanner(in);
		String readString = new String();
		String[] splitBySpace = new String[5];
		
		while(fileScanner.hasNextLine()) {
			readString = fileScanner.nextLine();
			splitBySpace = readString.split(" ");
			if(splitBySpace[0].equalsIgnoreCase("insert")) {
				Insert(splitBySpace);
			}
			if(splitBySpace[0].equalsIgnoreCase("delete")){
				Delete(splitBySpace);
			}
			if(splitBySpace[0].equalsIgnoreCase("report")) {
				GenerateReport(splitBySpace);
			}
			if(splitBySpace[0].equalsIgnoreCase("order:")) {
				String name = splitBySpace[1] + " " + splitBySpace[2].substring(0, splitBySpace[2].length()-1);
				Integer quantity = Integer.parseInt(splitBySpace[4]);
				NewOrder(name, splitBySpace[3].substring(0, splitBySpace[3].length()-1), quantity);
			}
		}
		fileScanner.close();
	}
	
	/**
	 * 
	 * @param name, name of order we want to create
	 * @param product, the desired product
	 * @param quantity, quantity of desired product
	 * @throws SQLException
	 * @throws FileNotFoundException
	 * @throws DocumentException
	 */
	
	private void NewOrder(String name, String product, Integer quantity) throws SQLException, FileNotFoundException, DocumentException {
		OrderDAO d = new OrderDAO(this.myCon);
		d.insertNewOrder(name, product, quantity, billNumber);
		billNumber++;
	}
	
	/**
	 * 
	 * @param myString, string read from file line
	 * @throws DocumentException
	 * @throws SQLException
	 * @throws IOException
	 */

	private void GenerateReport(String[] myString) throws DocumentException, SQLException, IOException {
		
		if(myString[1].equalsIgnoreCase("client")) {
			ClientDAO c = new ClientDAO(this.myCon);
			c.generateReportOnClients(clientReports);
			clientReports++;
		}
		
		if(myString[1].equalsIgnoreCase("product")) {
			ProductDAO p = new ProductDAO(this.myCon);
			p.generateReportOnProducts(productReports);
			productReports++;
		}
		if(myString[1].equalsIgnoreCase("order")) {
			OrderDAO o = new OrderDAO(this.myCon);
			o.generateReportOnOrders(orderReports);
			orderReports++;
		}
		
	}
	
	/**
	 * 
	 * @param myString, string read from input file line
	 * @throws SQLException
	 */
	
	private void Delete(String[] myString) throws SQLException {
		
		if(myString[1].equalsIgnoreCase("client:")) {
			ClientDAO c = new ClientDAO(this.myCon);
			String name  = myString[2] + " " + myString[3].substring(0, myString[3].length()-1);
			c.deleteClient(name, myString[4]);
		}
		
		if(myString[1].equalsIgnoreCase("product:")) {
			ProductDAO p = new ProductDAO(this.myCon);
			p.deleteProduct(myString[2]);
		}
		if(myString[1].equalsIgnoreCase("order:")) {
			Integer id = Integer.parseInt(myString[2]);
			OrderDAO o = new OrderDAO(this.myCon);
			o.DeleteOrder(id);
			
		}
		
	}
	
	/**
	 * 
	 * @param myString, string read from input file line
	 * @throws SQLException
	 */

	public void Insert(String[] myString) throws SQLException {
		
		if(myString[1].equalsIgnoreCase("client:")) {
			ClientDAO c = new ClientDAO(this.myCon);
			String name  = myString[2] + " " + myString[3].substring(0, myString[3].length()-1);
			c.insertNewClient(name, myString[4]);
		}
		if(myString[1].equalsIgnoreCase("product:")) {
			String name = myString[2].substring(0, myString[2].length()-1);
			Integer stock = Integer.parseInt(myString[3].substring(0, myString[3].length()-1));
			Double price = Double.parseDouble(myString[4]);
			ProductDAO p = new ProductDAO(this.myCon);
			p.InsertProduct(name, price, stock);
		}
		
	}
}
