package com.assignment3.databaseSimulator.model;

/**
 * 
 * @author Fodor Zsofi
 * Product entity
 *
 */

public class Product {
	
	/**
	 * Below are defined the columns of our product table
	 */
	
	private int productID;
	private String name;
	private double price;
	private int stock;
	
	public int getProductID() {
		return productID;
	}
	public void setProductID(int productID) {
		this.productID = productID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	

}
