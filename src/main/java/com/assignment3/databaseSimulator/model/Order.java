package com.assignment3.databaseSimulator.model;

/**
 * 
 * @author Fodor Zsofia
 * Order entity
 *
 */

public class Order {

	/**
	 * here are provided the columns of the Order table
	 */
	
	private int orderID;
	private int clientID;
	private int productID;
	private int quantity;
	
	public int getOrderID() {
		return orderID;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	public int getClientID() {
		return clientID;
	}
	public void setClientID(int clientID) {
		this.clientID = clientID;
	}
	public int getProductID() {
		return productID;
	}
	public void setProductID(int productID) {
		this.productID = productID;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
